\section{Announcements}

Assignment 2 was originally postponed to July 9th, and was now moved again to July 13th.
The due date for the course project is September 1st.

\section{Reminder}
We are discussing the following class of problems: We have several tasks that may run in parallel,
but we want some sort of ordering between them. That order is mainly important for performance reasons.

Our running example is SSSP (Single Source Shortest Path). Last time we've seen a parallelized version
of Dijkstra's algorithm that solves this problem. The algorithm knows the shortest path
for a ``frontier'' of nodes, which it keeps expanding. The parallelism here is possible because there
are usually several nodes at the boundary of the ``frontier''.

The parallelism is based on a multi-threaded queue, which we've seen several ways to implement.
We've seen the Skiplist, which has a problem scaling up (as we can see in figure \ref{fig:skiplist1}).
This problem is caused by the contention on the lists head, which contains the smallest element, and
is accessed by all threads. The program uses different processors before is uses
hyperthreading. When that hyperthreading comes into play, we see a slight improvement in performance.

\begin{figure}[h]
\centering
\caption{Initial Skiplist implementation\label{fig:skiplist1}}
\includegraphics[width=\textwidth]{images/skiplist1.png}
\end{figure}

When the program is configured to take advantage of hyperthreading earlier, we see some improvement
in those stages, but ultimately the algorithm still performs poorly (as seen in figure \ref{fig:skiplist2}).

\begin{figure}[h]
\centering
\caption{Modified Skiplist implementation\label{fig:skiplist2}}
\includegraphics[width=\textwidth]{images/skiplist2.png}
\end{figure}

So, how does one solve the contention problem? The answer is relaxation of the
data-structure properties. An example is the SprayList, which ``pops'' an element
of a polylogarithmic rank (instead of rank 0). This offers an improvement (figure \ref{fig:spraylist}).
We've also seen the multiqueue. This structure is more difficult to analyze,
but a recent article proposes an approximation.
According to the article, for a multiqueue of $n$ queues, the average rank is $O(n)$,
and the worst case is $O(n\log n)$.
Again, the relaxation offers improved performance (figure \ref{fig:multiqueue}), but we can't
tell if this is from better data-structure times or from improved ranks.

\begin{figure}[h]
\centering
\caption{SprayList performance\label{fig:spraylist}}
\includegraphics[width=\textwidth]{images/spraylist.png}
\end{figure}

\begin{figure}[h]
\centering
\caption{Multiqueue performance\label{fig:multiqueue}}
\includegraphics[width=\textwidth]{images/multiqueue.png}
\end{figure}

\section{OBIM - Ordered By Integer Metric}

\subsection{Outline}

The following data-structure comes from a more practical place. It offers no elaborate
theoretical analysis, but it is interesting nevertheless. Since this is a new solution,
a more critical viewing is advised. This is generally true for newer articles, and especially
empirical ones.

It is less general, since it relies on integer priorities (and not anything with full ordering).

Each priority has a bag (an unordered set), which contains the tasks with this priority. Those bags
are held in a global map that maps the integer priority to its assigned bag. Notice that since
there are very few constraints on the bag, it can be made very scalable.

Dequeuing is rather simple: extract a task from the highest priority non-empty bag. But how do we know
where to look? This is important because in the SSSP problem, the priorities increase as
the execution progresses (closer nodes are handled first). This means we start the search
from the middle of the list (not necessarily the center). Each thread has a local \texttt{startscan}
variable, an the search starts from the minimum of all of those variables (across all threads).
Since this is expensive, we will employ the following scheme: once every few dequeues,
reevaluate your \texttt{startscan} variable,
and normally first try to extract from the last used bag. Only if that bag is empty, scan for a new one.

Enqueuing is also simple. Find the appropriate bag for the new task (if it doesn't exist, create it),
and insert the new task into the bag. If the new priority is lower than \texttt{startscan}, update
it to that priority.

The detailed algorithm is shown in figure \ref{fig:obim-outline}.

\begin{figure}[H]
\centering
\begin{subfigure}[h]{0.5\textwidth}
\includegraphics[width=\textwidth]{images/obim-outline1.png}
\end{subfigure}%
\begin{subfigure}[h]{0.5\textwidth}
\includegraphics[width=\textwidth]{images/obim-outline2.png}
\end{subfigure}
\caption{OBIM outline}
\label{fig:obim-outline}
\end{figure}

The algorithm has room for improvement, but for SSSP it is definitely good
enough.

\subsection{Map implementation}

We have several possible problems: the global map might be a point of contention, and
implementing a concurrent map that supports sorted iteration is difficult.

How do we solve those problems?  Each thread has a local map that is a (possibly outdated)
snapshot of the global map. When all of the operation are done on the local map, the risk
of contention is gone, and iteration becomes possible, because the local map is single threaded.
It can be any data structure that can describe a sparse array (sorted vector, tree, etc.)

Now the problem becomes updating the maps, so that the local maps are close to the global map.
Eventually they should be the same. The global map is saved as a log of all of the insertions,
and updating the local maps becomes replaying the list of changes. Each insertion has
a version number that helps to know where replaying should start, and each thread remembers
the last seen version number. In order to avoid complications, all access to the global
map is done under a lock (as seen in figure \ref{fig:obim-globalmap}).

\begin{figure}[H]
\centering
\caption{OBIM global map implementation\label{fig:obim-globalmap}}
\includegraphics[width=\textwidth]{images/obim-globalmap.png}
\end{figure}

Although using a lock sounds bad, it doesn't impede performance, since most of the
time is not spent accessing the log. Updating the local map is done only when
a thread needs a bag for a priority not found in the global map.
Because of Amdahl's law, even though the global
map can be improved (for example, using a lock free list), it won't significantly
change the performance of the algorithm for the average input.

This design has several advantages:
\begin{itemize}
  \item The algorithm reduces communication between threads, which is useful for
  NUMA systems, in which accessing another processor's memory is significantly slower
  than accessing local memory. On the other hand, if the global map is implement using
  a more centralized solution (such as a skiplist), performance aren't significantly damaged.
  There might be a difference, but it is a small one, especially compared to what the article
  suggests.
  \item The algorithm doesn't require any concurrent data structures, since they are either
  local or used under a lock. This facilitates the use serial data structures, which are
  well tested and well optimized. For example, C++'s \texttt{std::dequeue} to implement
  the global log, and \texttt{std::vector} to implement the local map.
\end{itemize}

\subsection{Concurrent bag}

We don't have any requirement for global order. Each thread has a list, which
might be a serial locked list. Insertion to the bag is done to the thread's local
list. Extraction is first made from the local list, and if it's empty, then tasks
are stolen from another thread.

This simple implementation offers the required semantics, and if the bag is mostly
full, each thread uses primarily its local list.

\subsection{Semantics}

Is the OBIM a relaxed priority queue? On one hand it is not
a priority queue. If another thread inserts a task with a low priority,
it might not be visible to other threads for a long time. On the other
hand we at least don't actively avoid the best available element (like in
SprayList).

\subsection{Performance}

Compared to other data structures, the OBIM is much faster
(as seen in figure \ref{fig:obim-comparison}).

\begin{figure}[H]
\centering
\includegraphics[width=\textwidth]{images/obim-comparison.png}
\caption{Performance comparison}
\label{fig:obim-comparison}
\end{figure}

Let's try to break down the difference in performance, in figure \ref{fig:obim-breakdown}.

\begin{figure}[H]
\centering
\caption{Performance breakdown\label{fig:obim-breakdown}}
\includegraphics[width=\textwidth]{images/obim-breakdown.png}
\end{figure}

The number of cycles is normalized to the duration of the OBIM, and
is broken down to several components:
\begin{itemize}
  \item Empty work - Work done for nodes that don't improve upon the known distance.
  This is checked when \texttt{d > u.dist}.
  \item Bad work - Work done for nodes that will be reanalyzed later.
  \item Data structure usage (pushes and pops).
\end{itemize}

Each node can record how many cycles were spent iterating over it. If
the node was already analyzed, then the existing count is considered as bad work.
This is seen in figure \ref{fig:obim-badwork}.
\begin{figure}[H]
\centering
\caption{Bad work measurement\label{fig:obim-badwork}}
\includegraphics[width=\textwidth]{images/obim-badwork.png}
\end{figure}

Conclusions:
\begin{itemize}
  \item There is not much bad work or empty work. This means that the relaxations
  didn't cause significant performance penalties.
  \item Most of the performance costs were of pushes and pops, which are efficient
  in OBIM.
\end{itemize}

\subsection{Graph parallelism}

To show why the relaxations are insignificant, we'll examine the distribution
of task priorities over the execution of the algorithm.
The following figure (figure \ref{fig:obim-occupancy}), shows the average bag size
over the program's execution.
\begin{figure}[H]
\centering
\caption{Bag occupancy\label{fig:obim-occupancy}}
\includegraphics[width=\textwidth]{images/obim-occupancy.png}
\end{figure}

As we can see, the bags are very full throughout most of the execution,
which allows for better parallelism. 

On the other hand, the following figure (figure \ref{fig:obim-occupancy2}),
shows another graph, which has significantly lower bag occupancy.
This makes the graph less ``parallelized''. Now, instead
of mostly using local data-structures, each thread has to constantly
access the global map, and steal work from other threads.

\begin{figure}[H]
\centering
\caption{Bag occupancy 2\label{fig:obim-occupancy2}}
\includegraphics[width=\textwidth]{images/obim-occupancy2.png}
\end{figure}

In this case, the OBIM algorithm fails miserably, and the measurements time out.

How do we solve this issue? We can unite several priorities into one bag, using
the formula: $bag = priority / \delta$, for $\delta \ge 1$, which becomes a parameter
of the data structure.
While this improves the parallelism of the algorithm, it damages the rank produced
by the OBIM.
This causes the algorithm to compete with other implementations (figure \ref{fig:obim-comparison-delta}),
and even beat them for high thread counts. But, when we examine the performance breakdown,
we see a significant portion was dedicated to bad work (figure \ref{fig:obim-breakdown-delta}).

\begin{figure}[H]
\centering
\caption{Performance comparison with $\delta$\label{fig:obim-comparison-delta}}
\includegraphics[width=\textwidth]{images/obim-comparison-delta.png}
\end{figure}

\begin{figure}[H]
\centering
\caption{Performance breakdown with $delta$ \label{fig:obim-breakdown-delta}}
\includegraphics[width=\textwidth]{images/obim-breakdown-delta.png}
\end{figure}