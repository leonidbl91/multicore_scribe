\section{SWARM - Scalable Architecture For Ordered Parallelism}

\subsection{Main Idea}
Like in SSSP, we assume that the program consists of short tasks.
SWARM hardware executes those tasks speculatively (using transactional memory)
and out of order while the tasks are committed in-order. 

Every task in the program has a timestamp (TS). The timestamps specifies the order of correct execution.
An execution of a task can generate children tasks with equal or greater TS. 

In order to insert a new task to the system, a new hardware instruction was introduced:
$$
enqueue(fptr, ts, args...) 
$$

\subsection{Architecture Overview}
The solution is to be applicable to a large scale system, so SWARM uses a tiled architecture.
Each tile constitutes of several cores, each having
small, private, write-through L1 cache. All cores in a
tile share a per-tile L2 cache, and each tile has a slice
of a shared L3 cache. Tiles can communicate using the mesh (see \ref{fig:tiles}).


\begin{figure}[h]
	\center
	\includegraphics[width=\figwidth]{images/swarm_architecture.PNG}
	\caption{SWARM tile configuration}
	\label{fig:tiles}
\end{figure}

\textbf{LogTM style transactional memory.}
When a transaction is handled, it puts the data in place for faster commits. Aborts are more expensive and should be less often than commits.

\textbf{Per-tile task unit.}
Each tile features a task unit that queues, dispatches, and commits tasks.
The task unit consist of two queues: task queue and commit queue.

\subsection{Task Queue}
Task queue holds task descriptors (function pointer, TS, arguments) of tasks that are waiting be processed. 

When a core creates a new task, it sends the task to a random
target tile following the protocol in figure \ref{fig:task_enqueue}. Parent
and child track each other using task pointers. 

\begin{figure}[h]
	\center
	\includegraphics[width=\figwidth]{images/swarm_enqueue.PNG}
	\caption{Task enqueue protocol. Parent and child tracks each other using pointers.
	  A task pointer is simply the tuple (tile, task queue position).}
	\label{fig:task_enqueue}
\end{figure}

When a core wants to dequeue a task, it tries to dequeue the highest priority task from local task queue.

\subsection{Commit Queue}
The commit queue, holds a speculative state of tasks that finished execution and are waiting to be committed.
Every entry stores a R$\backslash$W set, undo log and the child pointers.

The R$\backslash$W set is used in order to determine memory data dependencies that are broken.
The undo log is used for undoing the task if it is aborted. Also, if a task is aborted,
its child should be aborted as well - hence we should track the child tasks. This flow will be explained in the next section.

\subsection{Ordered Commit}
Commit process should follow the TS order.
When the task are executed out of TS order, conflicts may arise when a
task accesses a line that was previously accessed by a
later-TS time task. When a conflict is detected, the later TS task should be aborted .

Let T1,T2 be two tasks with $ts(T2) > ts(T1)$.

The following  execution are recognized as conflicts - 
\begin{itemize}
	\item T1 RAW T2 \textrightarrow abort T2
	\item T1 WAW T2 \textrightarrow abort T2
\end{itemize}

While the following cases are considered correct - 
\begin{itemize}
	\item T2 RAW T1 
	\item T2 WAW T1 
\end{itemize}
However, if T1 aborts, T2 should be aborted as well, since
its execution is dependent upon speculative data produced by T1.

\subsection{Cascading Abort}
Upon a conflict, Swarm aborts the later task and all
its dependents: children tasks and other data-dependent tasks that have
accessed data written by the aborting task. 

Hardware aborts each task in three steps (applied recursively):

\begin{enumerate}
	\item Notify children to abort.
	\item Restore old memory values from the undo log.
	\item Free commit queue entry.
\end{enumerate}

An abort process can be seen in figure \ref{fig:abort}.
\begin{figure}[h]
	\center
	\includegraphics[width=\figwidth]{images/swarm_abort.PNG}
	\caption{A must abort after writing 0x10. A aborts the child D and grand child E. During rollback A also aborts C.}
	\label{fig:abort}
\end{figure}

\subsection{High Throughput Ordered Commit}
The system should support very short tasks (for example 64 cycles) and many cores.
In order to support high-throughput commit SWARM adapts a virtual time algorithm.

Tiles periodically send the smallest TS
of any unfinished task to an arbiter.
The arbiter computes the minimum
TS of all unfinished tasks, called the global
virtual time (GVT), and broadcasts it to all tiles. To
preserve ordering, only tasks with TS $<$ GVT
can commit.

Hopefully, we would commit many tasks at a time and that will supply the high throughput.
Also the commit is very fast due to the LogTM mentioned previously. GVT updates can be infrequent (e.g., 200 cycles).

\subsection{Results}
The SWARM architecture is testes with the configuration presented in \ref{fig:info}.
\begin{figure}[h]
	\center
	\includegraphics[width=\figwidth]{images/result_info.PNG}
	\caption{}
	\label{fig:info}
\end{figure}

\subsubsection{SWARM vs SW Implementation}
\begin{figure}[h]
	\center
	\includegraphics[width=\figwidth]{images/swarm_vs_sw.PNG}
	\caption{}
	\label{fig:sw_vs_swarm}
\end{figure}

The performance of the Swarm is compared to a
software-only versions of each benchmark. Each graph
shows the speedup of the Swarm and software-parallel
versions over the tuned serial version running on a system
of the same size, from 1 to 64 cores.

It can be easily noticed that SWARM if faster than the SW implementation.
Swarm outperforms the serial versions by 43x-117x,
and the software-parallel versions by 2.7x-18.2x.

It is very confusing to see the sssp test that shows a speedup of 117x over the serial version.
An important thing to know is that a single core SWARM implementation is $32\%$
faster that the single threaded SW implementation. But this does not explain such a speedup:
$
1.32 * 64 \approx 85
$.
There is no explanation in the paper why this speedup makes sense.

\subsubsection{Cycle breakdowns}
\begin{figure}[h]
	\center
	\includegraphics[width=\figwidth]{images/swarm_cycles.PNG}
	\caption{}
	\label{fig:swarm_cycles}
\end{figure}


Most time is spent in execution and not in commit (\ref{fig:swarm_cycles}) - the high throughput commit architecture works.

\subsubsection{Queue Occupancy}
\begin{figure}[h]
	\center
	\includegraphics[width=\textwidth]{images/swarm_queues.PNG}
	\caption{}
	\label{fig:swarm_queues}
\end{figure}

Both queues are often highly utilized (\ref{fig:swarm_queues}).
Commit queues can hold up to 1024 finished tasks
(64 per tile). On average, they hold from 200-800 entries. This shows that cores often execute tasks
out of order.
